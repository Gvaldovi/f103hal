/****************************************************************************************************/
/**
  \file         I2c.c
  \brief        
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version      0.0.1
  \date         Aug 16, 2016

  Program compiled with "COMPILER VERSION",
  Tested on "IK MASTER" board.

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include "I2c.h"

/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
volatile tsI2cChannel sI2cChannel[3];           		/* I2C channel status */
const tsI2cDriverCfg *psI2cDriverCfg;		/* Pointer to configuration */

u8 u8I2cTemp;

u8 u8I2cAddress;
u8 *pu8I2cData;
u16 u16I2cSize;
u8 u8I2cStatus;

/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        Variables EXTERN                                            *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Initialization of I2C driver.
* \author	Gerardo Valdovinos   
* \param    void
* \return   void
*****************************************************************************************************/
void vfnI2cInit(const tsI2cDriverCfg *psDriverCfg)
{
	u8 u8I2c;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Set pointer to configuration */
	psI2cDriverCfg = psDriverCfg;

	if(psI2cDriverCfg->u8Channels)
	{
		for(u8I2c = 0;u8I2c < psI2cDriverCfg->u8Channels;u8I2c++)
		{
			/* Set status structure */
			sI2cChannel[u8I2c].eChannel = psI2cDriverCfg->psChannelCfg[u8I2c].eChannel;
			sI2cChannel[u8I2c].u8Status = I2C_ON;

			/* Turn on peripheral clock */
			I2C_APB1ENR(u8I2c);

			/* Interrupts enabled and freq 42MHz */
			I2C_CR2_SET(u8I2c, 	I2C_CR2_ITBUFEN |
								I2C_CR2_ITEVTEN |
								I2C_CR2_ITERREN |
								I2C_CR2_FREQ_5 |
								I2C_CR2_FREQ_3 |
								I2C_CR2_FREQ_1);

			/* Configure clock */
			I2C_CCR_SET(u8I2c, psI2cDriverCfg->psChannelCfg[u8I2c].u16Clock);

			/* Configure rise time. Constant for a 42MHz */
			I2C_TRISE_SET(u8I2c, 43);

			/* Configure SCL pin */
			vfnGpioCfg((tsGpioChannelCfg*)&psI2cDriverCfg->psChannelCfg[u8I2c].sGpioChannelCfgSCL);

			/* Configure SDA pin */
			vfnGpioCfg((tsGpioChannelCfg*)&psI2cDriverCfg->psChannelCfg[u8I2c].sGpioChannelCfgSDA);

			/* Activate interrupts */
			NVIC_InitStructure.NVIC_IRQChannel = psI2cDriverCfg->psChannelCfg[u8I2c].tIRQEvents;
			NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 7;
			NVIC_InitStructure.NVIC_IRQChannelSubPriority = u8I2c;
			NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
			NVIC_Init(&NVIC_InitStructure);
			NVIC_InitStructure.NVIC_IRQChannel = psI2cDriverCfg->psChannelCfg[u8I2c].tIRQErrors;
			NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 7;
			NVIC_InitStructure.NVIC_IRQChannelSubPriority = u8I2c + 3;
			NVIC_Init(&NVIC_InitStructure);
		}
	}
}

/*****************************************************************************************************
* \brief    I2C is busy?.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u8 u8fnI2cIsBusy(teI2cChannel Channel)
{
	if(sI2cChannel[Channel].u8Status == I2C_ON)
		return FALSE;
	else
		return TRUE;
}

/*****************************************************************************************************
* \brief    Enable I2C channel.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnI2cEnable(teI2cChannel Channel)
{
	I2C_CR1_SET(Channel, I2C_CR1_PE);
}

/*****************************************************************************************************
* \brief    Disable I2C channel.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnI2cDisable(teI2cChannel Channel)
{
	I2C_CR1_CLEAR(Channel, I2C_CR1_PE);
}

///*****************************************************************************************************
//* \brief    Get status of channel.
//* \author	Gerardo Valdovinos
//* \param    void
//* \return   void
//*****************************************************************************************************/
//u8 u8fnI2cGetStatus(teI2cChannel Channel)
//{
//	return (sI2cChannel[Channel].u8Status);
//}

/*****************************************************************************************************
* \brief    I2C read function.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u8 u8fnI2cRead(teI2cChannel Channel, u8 u8Address, u8 *pu8Data, u16 u16Size)
{
	if(sI2cChannel[Channel].u8Status != I2C_BUSY)
	{
		sI2cChannel[Channel].u8Address = (u8)((u8Address << 1) | 0x01);
		sI2cChannel[Channel].pu8Data = pu8Data;
		sI2cChannel[Channel].u16Size = u16Size;
		sI2cChannel[Channel].u8Status = I2C_BUSY;

		/* Set ack and start bit */
		I2C_CR1_SET(Channel, I2C_CR1_ACK |I2C_CR1_START);

		return 1;
	}
	return 0;
}

/*****************************************************************************************************
* \brief    I2C Write function.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u8 u8fnI2cWrite(teI2cChannel Channel, u8 u8Address, u8 *pu8Data, u16 u16Size)
{
	if(sI2cChannel[Channel].u8Status != I2C_BUSY)
	{
		sI2cChannel[Channel].u8Address = (u8)((u8Address << 1) | 0x00);
		sI2cChannel[Channel].pu8Data = pu8Data;
		sI2cChannel[Channel].u16Size = u16Size;
		sI2cChannel[Channel].u8Status = I2C_BUSY;

		/* Set ack and start bit */
		I2C_CR1_SET(Channel, I2C_CR1_ACK |I2C_CR1_START);

		return 1;
	}
	return 0;
}

/*****************************************************************************************************
* \brief    I2C1 Interrupt Routine Service for events
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
*****************************************************************************************************/
void I2C1_EV_IRQHandler(void)
{
	/* Start detection */
	if(I2C_SR1_GET(eI2C_CH1, I2C_SR1_SB))
	{
		/* Write address */
		I2C_DR_WRITE(eI2C_CH1, sI2cChannel[eI2C_CH1].u8Address);
	}

	/* Address detection */
	if(I2C_SR1_GET(eI2C_CH1, I2C_SR1_ADDR))
	{
		if(sI2cChannel[eI2C_CH1].u16Size == 1)
			I2C_CR1_CLEAR(eI2C_CH1, I2C_CR1_ACK);

		/* Erase ADDR flag */
		u8I2cTemp = I2C_SR2_READ(eI2C_CH1);
	}

	/* Transmit buffer empty */
	if(I2C_SR1_GET(eI2C_CH1, I2C_SR1_TXE))
	{
		if(sI2cChannel[eI2C_CH1].u16Size--)
			I2C_DR_WRITE(eI2C_CH1, *sI2cChannel[eI2C_CH1].pu8Data++);
		else
		{
			I2C_CR1_SET(eI2C_CH1, I2C_CR1_STOP);  // Finish transmission
			sI2cChannel[eI2C_CH1].u8Status = I2C_ON;
		}
	}

	/* Receive buffer not empty */
	if(I2C_SR1_GET(eI2C_CH1, I2C_SR1_RXNE))
	{
		*sI2cChannel[eI2C_CH1].pu8Data++ = I2C_DR_READ(eI2C_CH1);

		sI2cChannel[eI2C_CH1].u16Size--;
		if(sI2cChannel[eI2C_CH1].u16Size <= 1)
		{
			I2C_CR1_CLEAR(eI2C_CH1, I2C_CR1_ACK);
			I2C_CR1_SET(eI2C_CH1, I2C_CR1_STOP);  // Finish transmission
			sI2cChannel[eI2C_CH1].u8Status = I2C_ON;
		}
	}

	/* Byte transfer finish */
	if(I2C_SR1_GET(eI2C_CH1, I2C_SR1_BTF))
	{
		if(I2C_SR1_GET(eI2C_CH1, I2C_SR1_TXE))
			I2C_DR_WRITE(eI2C_CH1, u8I2cTemp);

		if(I2C_SR1_GET(eI2C_CH1, I2C_SR1_RXNE))
			u8I2cTemp = I2C_DR_READ(eI2C_CH1);

		I2C_CR1_SET(eI2C_CH1, I2C_CR1_STOP);  // Finish transmission
		sI2cChannel[eI2C_CH1].u8Status = I2C_ON;
	}
}

/*****************************************************************************************************
* \brief    I2C1 Interrupt Routine Service for error
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
*****************************************************************************************************/
void I2C1_ER_IRQHandler(void)
{
	I2C_SR1_CLEAR(eI2C_CH1, I2C_SR1_BERR);
	I2C_SR1_CLEAR(eI2C_CH1, I2C_SR1_ARLO);
	I2C_SR1_CLEAR(eI2C_CH1, I2C_SR1_AF);
	I2C_SR1_CLEAR(eI2C_CH1, I2C_SR1_OVR);
	I2C_SR1_CLEAR(eI2C_CH1, I2C_SR1_PECERR);
	I2C_SR1_CLEAR(eI2C_CH1, I2C_SR1_TIMEOUT);
	I2C_SR1_CLEAR(eI2C_CH1, I2C_SR1_SMBALERT);

	I2C_CR1_SET(eI2C_CH1, I2C_CR1_STOP);
//	sI2cChannel[eI2C_CH1].u8Status = I2C_NO_ACK;
}
/***************************************End of Functions Definition**********************************/
