/****************************************************************************************************/
/**
  \file         I2cCfg.h
  \brief        
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version      
  \date         Aug 19, 2016

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/
#ifndef __I2C_CFG_H
#define	__I2C_CFG_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "Typedefs.h"
#include "Gpio.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
#define I2C_50KHZ		420
#define I2C_100KHZ		210
#define I2C_150KHZ		140
#define I2C_200KHZ		105
/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef enum
{
	eI2C_CH1 = 0,
	eI2C_CH2,
	eI2C_CH3
}teI2cChannel;

typedef struct
{
	teI2cChannel				eChannel;			/* I2C channel */
	u16							u16Clock;			/* Clock speed */
	IRQn_Type           		tIRQEvents;         /* Interrupt request routine for events */
	IRQn_Type           		tIRQErrors;         /* Interrupt request routine for errors */

	tsGpioChannelCfg			sGpioChannelCfgSCL;	/* Gpio SCL configuration */
	tsGpioChannelCfg			sGpioChannelCfgSDA;	/* Gpio SDA configuration */
}tsI2cChannelCfg;

typedef struct
{
	u8                          u8Channels; 		/* Number of channels */
	const tsI2cChannelCfg      *psChannelCfg;  		/* Channels configuration pointer */
}tsI2cDriverCfg;
/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
extern const tsI2cDriverCfg sI2cDriverCfg;

#endif	/* __I2C_CFG_H */
/***************************************End of File**************************************************/
