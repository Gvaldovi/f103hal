/****************************************************************************************************/
/**
  \file         Spi.c
  \brief
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version      0.0.1
  \date         Mar 21, 2017

  Program compiled with "COMPILER VERSION",
  Tested on "IK MASTER" board.

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include "Spi.h"

/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/******************************************
 * ADDRESS OF SPI1 TO SPI3. DO NOT MOVE *
 *****************************************/
const u32 au32SpiAddress[] =
{
		0x40013000,
		0x40003800,
		0x40003C00
};

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
volatile tsSpiChannel sSpiChannel[eSPI_CH_MAX];		/* SPI channel status */
const tsSpiDriverCfg *psSpiDriverCfg;		/* Pointer to configuration */

/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        Variables EXTERN                                            *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Initialization of SPI driver.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnSpiInit(const tsSpiDriverCfg *psDriverCfg)
{
	u8 u8Spi;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Set pointer to configuration */
	psSpiDriverCfg = psDriverCfg;

	if(psSpiDriverCfg->u8Channels)
	{
		/* High NSS for avoiding problems */
		vfnGpioSet(eGPIO_PORTA, 4);

		for(u8Spi = 0;u8Spi < psSpiDriverCfg->u8Channels;u8Spi++)
		{
			/* Set status structure */
			sSpiChannel[u8Spi].eChannel = psSpiDriverCfg->psChannelCfg[u8Spi].eChannel;
			sSpiChannel[u8Spi].u8Status = SPI_ON;

			/* Turn on peripheral clock */
			if(u8Spi == 0)
			{
				SPI_APB2ENR(u8Spi);

				/* Select baudrate */
				SPI_BR_WRITE(u8Spi, psSpiDriverCfg->psChannelCfg[u8Spi].eBaud);
			}
			else
			{
				SPI_APB1ENR(u8Spi);

				/* Select baudrate */
				SPI_BR_WRITE(u8Spi, (psSpiDriverCfg->psChannelCfg[u8Spi].eBaud - 1));
			}

			/* Configure channel */
			SPI_CR1_SET(u8Spi, 	SPI_CR1_CPOL |
								SPI_CR1_CPHA |
								SPI_CR1_MSTR |
								SPI_CR1_SSM	 |
								SPI_CR1_SSI  );

			/* Configure SCL pin */
			vfnGpioCfg((tsGpioChannelCfg*)&psSpiDriverCfg->psChannelCfg[u8Spi].sMOSI);

			/* Configure SDA pin */
			vfnGpioCfg((tsGpioChannelCfg*)&psSpiDriverCfg->psChannelCfg[u8Spi].sMISO);

			/* Configure SDA pin */
			vfnGpioCfg((tsGpioChannelCfg*)&psSpiDriverCfg->psChannelCfg[u8Spi].sSCK);

			/* Activate interrupts */
			NVIC_InitStructure.NVIC_IRQChannel = psSpiDriverCfg->psChannelCfg[u8Spi].tIRQ;
			NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 8;
			NVIC_InitStructure.NVIC_IRQChannelSubPriority = u8Spi;
			NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
			NVIC_Init(&NVIC_InitStructure);
		}
	}
}

/*****************************************************************************************************
* \brief    SPI deinit.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnSpiDeInit(teSpiChannel Channel)
{
	/* Turn off all registers */
	SPI_CR1_WRITE(Channel, 0);
	SPI_CR2_WRITE(Channel, 0);
}

/*****************************************************************************************************
* \brief    SPI is busy?.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u8 u8fnSpiIsBusy(teSpiChannel Channel)
{
	if(sSpiChannel[Channel].u8Status == SPI_ON)
		return FALSE;
	else
		return TRUE;
}

/*****************************************************************************************************
* \brief    Enable SPI channel.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnSpiEnable(teSpiChannel Channel)
{
	SPI_CR1_SET(Channel, SPI_CR1_SPE);
}

/*****************************************************************************************************
* \brief    Disable SPI channel.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnSpiDisable(teSpiChannel Channel)
{
	SPI_CR1_CLEAR(Channel, SPI_CR1_SPE);
}

/*****************************************************************************************************
* \brief    Set SPI Tx callback.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnSpiSetCallback(teSpiChannel Channel, vpfn pfnCallback)
{
	sSpiChannel[Channel].pfnTxCallback = pfnCallback;
}

/*****************************************************************************************************
* \brief    SPI read function.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u8 u8fnSpiSend(teSpiChannel Channel, u16 u16Size, u8 *pu8Data)
{
	if(sSpiChannel[Channel].u8Status != SPI_BUSY)
	{
		sSpiChannel[Channel].pu8Data = pu8Data;
		sSpiChannel[Channel].u16Size = u16Size;
		sSpiChannel[Channel].u8Status = SPI_BUSY;

		/* Configure interrupts */
		SPI_CR2_SET(Channel, SPI_CR2_RXNEIE);

		/* Send first byte */
		SPI_DR_WRITE(Channel, *pu8Data);

		return 1;
	}
	return 0;
}


/*****************************************************************************************************
* \brief    SPI1 Interrupt Routine Service for events
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
*****************************************************************************************************/
void SPI1_IRQHandler(void)
{
    /* Reception of a byte */
	if(SPI_SR_GET(eSPI_CH1, SPI_SR_RXNE))
	{
    	/* Copy byte received */
    	*sSpiChannel[eSPI_CH1].pu8Data++ = SPI_DR_READ(eSPI_CH1);

    	if(--sSpiChannel[eSPI_CH1].u16Size == 0)
    	{
    		SPI_CR2_CLEAR(eSPI_CH1, SPI_CR2_RXNEIE);
    		sSpiChannel[eSPI_CH1].u8Status = SPI_ON;

    		if(sSpiChannel[eSPI_CH1].pfnTxCallback != NULL)
    			sSpiChannel[eSPI_CH1].pfnTxCallback();
    	}
    	else
    		SPI_DR_WRITE(eSPI_CH1, *sSpiChannel[eSPI_CH1].pu8Data);
	}

}

/***************************************End of Functions Definition**********************************/
