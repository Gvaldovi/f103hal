/****************************************************************************************************/
/**
  \file         GpioCfg.h
  \brief        
  \author       Gerardo Valdovinos
  \project      Bluepill
  \version      
  \date         Mar 15, 2019

  THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/
#ifndef __GPIO_CFG_H
#define	__GPIO_CFG_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "Typedefs.h"

/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef enum
{
	eGPIO_PORTA = 0,
	eGPIO_PORTB,
	eGPIO_PORTC
}teGpioPort;

typedef enum
{
	eGPIO_OUT_PP = 0,
	eGPIO_OUT_OD,
	eGPIO_AF_PP,
	eGPIO_AF_OD,
	eGPIO_IN_FLOAT,
	eGPIO_IN_PU,
	eGPIO_IN_PD,
	eGPIO_IN_ANALOG
}teGpioCfg;

typedef struct
{
	teGpioPort		ePort	: 4;				/* Port */
	u8				u8Pin	: 4;				/* Pin number */
	teGpioCfg		eCfg	: 4;				/* Configuration */
	u8				u8AF	: 4;				/* Alternate function */
}tsGpioChannelCfg;

typedef struct
{
	u8                  		u8Channels; 	/* Number of channels */
	const tsGpioChannelCfg		*psChannelCfg;  /* Channels configuration pointer */
}tsGpioDriverCfg;

extern const tsGpioDriverCfg sGpioDriverCfg;
/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/

#endif	/* __GPIO_CFG_H */
/***************************************End of File**************************************************/
