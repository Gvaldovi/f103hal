/****************************************************************************************************/
/**
  \file         UART.h
  \brief
  \author       Gerardo Valdovinos
  \project      Bluepill
  \version
  \date         Mar 15, 2019

  THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/
#ifndef __UART_H /*Duplicated Includes Prevention*/
#define __UART_H
/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "UartCfg.h"

/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Uart States~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define UART_ON			                0
#define UART_OFF		                1
#define UART_TX_BUSY	                2
#define UART_RX_BUSY	                3
#define UART_OVERRUN	                4

/* Devices */
//#define UART_USB						eUART_CH1
//#define UART_MODEM						eUART_CH2
//#define UART_OPTIC						eUART_CH3
//#define UART_METERS						eUART_CH4
//#define UART_AUX						eUART_CH5
//#define UART_RADIO						eUART_CH6

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/
/* The offsets were taken from data sheet of STM32F407 pag. 1713 */
#define UART_SR(x)								*(u32*)(au32UartAddress[x] + 0x00)
#define UART_SR_WRITE(x, y)                     UART_SR(x) = y;
#define UART_SR_SET(x, y)                       UART_SR(x) |= y;
#define UART_SR_CLEAR(x, y)                     UART_SR(x) &= ~y;
#define UART_SR_GET(x, y)                       ((UART_SR(x) & y) == y)

#define UART_DR(x)                              *(u32*)(au32UartAddress[x] + 0x04)
#define UART_DR_WRITE(x, y)                     UART_DR(x) = y
#define UART_DR_READ(x)                     	(u8)UART_DR(x)
#define UART_DR_SET(x, y)                       UART_DR(x) |= y
#define UART_DR_CLEAR(x, y)                     UART_DR(x) &= ~y
#define UART_DR_GET(x, y)                       ((UART_DR(x) & y) == y)

#define UART_BRR(x)                             *(u32*)(au32UartAddress[x] + 0x08)
#define UART_BRR_WRITE(x, y)                    UART_BRR(x) = y
#define UART_BRR_SET(x, y)                      UART_BRR(x) |= y
#define UART_BRR_CLEAR(x, y)                    UART_BRR(x) &= ~y
#define UART_BRR_GET(x, y)                      ((UART_BRR(x) & y) == y)

#define UART_CR1(x)                             *(u32*)(au32UartAddress[x] + 0x0C)
#define UART_CR1_WRITE(x, y)                    UART_CR1(x) = y
#define UART_CR1_SET(x, y)                      UART_CR1(x) |= y
#define UART_CR1_CLEAR(x, y)                    UART_CR1(x) &= ~y
#define UART_CR1_GET(x, y)                      ((UART_CR1(x) & y) == y)

#define UART_CR2(x)                             *(u32*)(au32UartAddress[x] + 0x10)
#define UART_CR2_WRITE(x, y)                    UART_CR2(x) = y
#define UART_CR2_SET(x, y)                      UART_CR2(x) |= y
#define UART_CR2_CLEAR(x, y)                    UART_CR2(x) &= ~y
#define UART_CR2_GET(x, y)                      ((UART_CR2(x) & y) == y)

#define UART_CR3(x)                             *(u32*)(au32UartAddress[x] + 0x14)
#define UART_CR3_SET(x, y)                      UART_CR3(x) |= y
#define UART_CR3_CLEAR(x, y)                    UART_CR3(x) &= ~y
#define UART_CR3_WRITE(x, y)                    UART_CR3(x) = y

#define UART_APB2ENR(x)							RCC->APB2ENR |= (u32)(1 << 14)
#define UART_APB1ENR(x)							RCC->APB1ENR |= (u32)(1 << (17 + x))

//#define UART_NVIC_SET_PRIORITY(x)				NVIC_SetPriority(USART1_IRQn + x, 0)
//#define UART_NVIC_ENABLE_IRQ(x)                 NVIC_EnableIRQ(USART1_IRQn + x)

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef struct
{
  teUartChannel                 eChannel;               /* Uart channel */
  u8                            u8Status;               /* Status of driver */
  u16                           u16TxSize;            	/* Tx buffer size */
  u16                           u16RxSize;            	/* Rx buffer size */
  u8*                           pu8RxBuffer;            /* Pointer to Rx buffer */
  u8*                           pu8TxBuffer;            /* Pointer to Tx buffer */
  vpfn                          pfnRxCallback;          /* Reception callback */
  vpfn                          pfnTxCallback;          /* Transmission callback */
}tsUartChannel;

typedef enum
{
	eUART_OFF = 0,
	eUART_ON
}teUartState;


/*****************************************************************************************************
*                                       Declaration of VARIABLEs                                     *
*****************************************************************************************************/

/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnUartInit(const tsUartDriverCfg *UartDriverCfg);
void vfnUartDeInit(teUartChannel Channel);
u8 u8fnUartIsBusy(teUartChannel Channel);

void vfnUartPower(teUartChannel Channel, teUartState eState);
void vfnUartDmaRx(teUartChannel Channel, teUartState eState);
void vfnUartSetCallbacks(teUartChannel Channel, vpfn pfnTxCallback, vpfn pfnRxCallback);
void vfnUartBaud(teUartChannel Channel, u16 u16Baud);
void vfnUartReset(teUartChannel Channel);
u16 u16fnUartRead(teUartChannel Channel, u8 *pu8Data);
u16 u16fnUartReadSize(teUartChannel Channel);
u8 u8fnUartWrite(teUartChannel Channel, u8 *pu8Data, u16 u16Size);

/****************************************************************************************************/
#endif /* __UART_H */
/***************************************End of File**************************************************/
