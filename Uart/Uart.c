/****************************************************************************************************/
/**
    \file       Uart.c
  \brief
  \author       Gerardo Valdovinos Villalobos
  \project      Bluepill
  \version
  \date         Mar 15, 2019

  THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                           Include files                                            *
*****************************************************************************************************/
#include "Uart.h"
#include "string.h"
/*****************************************************************************************************
*                                               #DEFINEs                                             *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                      Declaration of module TYPEs                                   *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        Definition of VARIABLEs                                     *
*****************************************************************************************************/
tsUartChannel sUartChannel[eUART_CH_MAX];     /* Uart channel status */
const tsUartDriverCfg *psUartDriverCfg;				/* Pointer to configuration */
u8 u8UartTemp;

/****************************************************************************************************
*                                    Declaration of module FUNCTIONs                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        Definition of CONSTANTs                                     *
*****************************************************************************************************/

/******************************************
 * ADDRESS OF UART1 TO UART6. DO NOT MOVE *
 *****************************************/
const u32 au32UartAddress[] =
{
        0x40013800,
        0x40004400,
        0x40004800
};
/*****************************************************************************************************
*                                         Definition of FUNCTIONS                                    *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    USART/UARTS Modules initialization.
* \author   Gerardo Valdovinos Villalobos
* \param    const tsUartDriverCfg *UartDriverCfg - Pointer which contains USART/UARTS configurations.
*           user configurations
* \return   void
*****************************************************************************************************/
void vfnUartInit(const tsUartDriverCfg *UartDriverCfg)
{
	u8 u8Uart;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Set pointer to configuration */
	psUartDriverCfg = UartDriverCfg;

	/* if there are channels */
	if(psUartDriverCfg->u8Channels)
	{
		/* Configure Uart channels */
		for(u8Uart = 0; u8Uart < psUartDriverCfg->u8Channels; u8Uart++)
		{
			/* Fill internal structures */
			sUartChannel[u8Uart].eChannel = u8Uart;
			sUartChannel[u8Uart].u8Status = UART_OFF;
		    sUartChannel[u8Uart].u16TxSize = 0;
			sUartChannel[u8Uart].u16RxSize = 0;
			sUartChannel[u8Uart].pu8RxBuffer = psUartDriverCfg->psChannelCfg[u8Uart].pu8RxBuffer;
			sUartChannel[u8Uart].pu8TxBuffer = NULL;

			/* Turn on Uart clock */
			if(u8Uart == 0)
				UART_APB2ENR(u8Uart);
			else
				UART_APB1ENR(u8Uart);

			/* Uart parity */
			if(psUartDriverCfg->psChannelCfg[u8Uart].eParity != eUART_PARITY_NO)
			{
				UART_CR1_SET(u8Uart, USART_CR1_PCE);

				if(psUartDriverCfg->psChannelCfg[u8Uart].eParity == eUART_PARITY_ODD)
					UART_CR1_SET(u8Uart, USART_CR1_PS);
			}

			/* Uart word length */
			if(psUartDriverCfg->psChannelCfg[u8Uart].eWordLength == eUART_LENGTH_9)
				UART_CR1_SET(u8Uart, USART_CR1_M);

			/* Uart baud rate */
			UART_BRR_WRITE(u8Uart, psUartDriverCfg->psChannelCfg[u8Uart].u16Baud);

			/* Other configurations */
			UART_CR1_SET(u8Uart, USART_CR1_RE | USART_CR1_TE | USART_CR1_RXNEIE);

			/* Clear status register */
			UART_SR_WRITE(u8Uart, 0);

			/* Configure GPIO for Uart Tx */
			vfnGpioCfg((tsGpioChannelCfg*)&psUartDriverCfg->psChannelCfg[u8Uart].sGpioChannelCfgTx);
			/* Configure GPIO for Uart Rx */
			vfnGpioCfg((tsGpioChannelCfg*)&psUartDriverCfg->psChannelCfg[u8Uart].sGpioChannelCfgRx);

			/* Activate interrupts */
			NVIC_InitStructure.NVIC_IRQChannel = psUartDriverCfg->psChannelCfg[u8Uart].tIRQ;
			NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 6;
			NVIC_InitStructure.NVIC_IRQChannelSubPriority = u8Uart;
			NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
			NVIC_Init(&NVIC_InitStructure);
		}
	}
}

/*****************************************************************************************************
* \brief    Usart deinit.
* \author   Gerardo Valdovinos
* \param    teUartChannels teUart - Module to enable.
* \return   void
*****************************************************************************************************/
void vfnUartDeInit(teUartChannel Channel)
{
	/* Turn off all registers */
	UART_CR1_WRITE(Channel, 0);
	UART_CR2_WRITE(Channel, 0);
	UART_CR3_WRITE(Channel, 0);
}

/*****************************************************************************************************
* \brief    Usart is busy?
* \author   Gerardo Valdovinos
* \param    teUartChannels teUart - Module to enable.
* \return   void
*****************************************************************************************************/
u8 u8fnUartIsBusy(teUartChannel Channel)
{
	if(sUartChannel[Channel].u8Status == UART_ON)
		return FALSE;
	else
		return TRUE;
}

/*****************************************************************************************************
* \brief    Usart enable/disable
* \author   Gerardo Valdovinos
* \param    teUartChannels teUart - Module to enable.
* \return   void
*****************************************************************************************************/
void vfnUartPower(teUartChannel Channel, teUartState eState)
{
	if(eState == eUART_ON)
	{
		UART_CR1_SET(Channel, USART_CR1_UE);
		sUartChannel[Channel].u8Status = UART_ON;
	}
	else
	{
		UART_CR1_CLEAR(Channel, USART_CR1_UE);
		sUartChannel[Channel].u8Status = UART_OFF;
	}
}

/*****************************************************************************************************
* \brief    Usart DMA enable/disable
* \author   Gerardo Valdovinos
* \param    teUartChannels teUart - Module to disable.
* \return   void
*****************************************************************************************************/
void vfnUartDmaRx(teUartChannel Channel, teUartState eState)
{
	if(eState == eUART_ON)
	{
		UART_CR3_SET(Channel, USART_CR3_DMAR);
		UART_CR1_CLEAR(Channel, USART_CR1_RXNEIE);
		UART_CR1_SET(Channel, USART_CR1_IDLEIE);
	}
	else
	{
		UART_CR3_CLEAR(Channel, USART_CR3_DMAR);
		UART_CR1_SET(Channel, USART_CR1_RXNEIE);
		UART_CR1_CLEAR(Channel, USART_CR1_IDLEIE);
	}
}

/*****************************************************************************************************
* \brief    Usart set Callbacks.
* \author   Gerardo Valdovinos
* \param    teUartChannels teUart - Module to disable.
* \return   void
*****************************************************************************************************/
void vfnUartSetCallbacks(teUartChannel Channel, vpfn pfnTxCallback, vpfn pfnRxCallback)
{
	sUartChannel[Channel].pfnTxCallback = pfnTxCallback;
	sUartChannel[Channel].pfnRxCallback = pfnRxCallback;
}

/*****************************************************************************************************
* \brief    Usart change baudrate
* \author   Gerardo Valdovinos
* \param    teUartChannels Channel - Channel to set baud.
* \return   void
*****************************************************************************************************/
void vfnUartBaud(teUartChannel Channel, u16 u16Baud)
{
	/* Uart baud rate */
	UART_BRR_WRITE(Channel, u16Baud);
}

/*****************************************************************************************************
* \brief    Reset uart buffer.
* \author   Gerardo Valdovinos
* \param    teUartChannels Channel - Module to reset.
* \return   void
*****************************************************************************************************/
void vfnUartReset(teUartChannel Channel)
{
	/* Copy values */
	sUartChannel[Channel].pu8RxBuffer = psUartDriverCfg->psChannelCfg[Channel].pu8RxBuffer;
	sUartChannel[Channel].u16RxSize = 0;
	UART_SR_WRITE(Channel, 0);
}

/*****************************************************************************************************
* \brief    Get pointer to Rx buffer to a selected USART/UART module.
* \author   Gerardo Valdovinos
* \param    teUartChannels Channel - Module to be written.
* \return   void
*****************************************************************************************************/
u16 u16fnUartRead(teUartChannel Channel, u8* pu8Data)
{
	u16 u16Size;

	/* Copy values */
	sUartChannel[Channel].pu8RxBuffer = psUartDriverCfg->psChannelCfg[Channel].pu8RxBuffer;
	u16Size = sUartChannel[Channel].u16RxSize;
	sUartChannel[Channel].u16RxSize = 0;

	memcpy(&pu8Data[0], &sUartChannel[Channel].pu8RxBuffer[0], u16Size);

	return u16Size;
}

/*****************************************************************************************************
* \brief    Get size of data in Rx buffer to a selected USART/UART module.
* \author   Gerardo Valdovinos
* \param    teUartChannels Channel.
* \return   void
*****************************************************************************************************/
u16 u16fnUartReadSize(teUartChannel Channel)
{
	return sUartChannel[Channel].u16RxSize;
}

/*****************************************************************************************************
* \brief    Write buffer to a selected USART/UART module.
* \author   Gerardo Valdovinos
* \param    teUartChannels teUart - Module to be written.
*           u8* pu8Data - pointer to buffer to be written
*           u16 u16Size - length of buffer to be written
* \return   void
*****************************************************************************************************/
u8 u8fnUartWrite(teUartChannel Channel, u8 *pu8Data, u16 u16Size)
{
	/* Verify if Uart channel is not busy */
	if( sUartChannel[Channel].u8Status == UART_ON &&
		pu8Data != NULL &&
		u16Size != 0)
	{
		sUartChannel[Channel].u8Status = UART_TX_BUSY;

		/* Copy variables */
		sUartChannel[Channel].u16TxSize = u16Size;
		sUartChannel[Channel].pu8TxBuffer = pu8Data;

		/* Turn on Tx interrupt */
		UART_CR1_SET(Channel, USART_CR1_TXEIE);

		return TRUE;
	}
	return FALSE;
}

/*****************************************************************************************************
* \brief    USART1 Interrupt Routine Service
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
* \notes
*****************************************************************************************************/
void USART1_IRQHandler(void)
{

	if(UART_SR_GET(eUART_CH1, USART_SR_RXNE))
	{
		/* It was a Rx interrupt */
		if( sUartChannel[eUART_CH1].pu8RxBuffer != NULL &&
			sUartChannel[eUART_CH1].u16RxSize < UART_CH1_BUFFER_SIZE)
		{
			*sUartChannel[eUART_CH1].pu8RxBuffer++ = UART_DR_READ(eUART_CH1);// USART6->DR;
			sUartChannel[eUART_CH1].u16RxSize++;
			UART_CR1_SET(eUART_CH1, USART_CR1_IDLEIE);
		}
	}
	else if(UART_SR_GET(eUART_CH1, USART_SR_IDLE) )
	{
		/* Channel is IDLE */

		/* Sequence to turn off IDLE flag */
		u8UartTemp = UART_DR_READ(eUART_CH1);

		/* Rx callback */
		if(sUartChannel[eUART_CH1].pfnRxCallback != NULL)
			sUartChannel[eUART_CH1].pfnRxCallback();

		UART_CR1_CLEAR(eUART_CH1, USART_CR1_IDLEIE);
	}
	else if(UART_SR_GET(eUART_CH1, USART_SR_TXE) )
	{
		/* It was a Tx interrupt. Tx Buffer empty */

		/* Review if there are more bytes to send */
		if(sUartChannel[eUART_CH1].u16TxSize-- > 0)
		{
			if(sUartChannel[eUART_CH1].pu8TxBuffer != NULL)
				UART_DR_WRITE(eUART_CH1, *sUartChannel[eUART_CH1].pu8TxBuffer++);
		}
		else
		{
			/* Turn off Tx interrupt */
			UART_CR1_CLEAR(eUART_CH1, USART_CR1_TXEIE);

			/* Change status of driver */
			sUartChannel[eUART_CH1].u8Status = UART_ON;
			sUartChannel[eUART_CH1].pu8TxBuffer = NULL;

			/* Tx callback */
			if(sUartChannel[eUART_CH1].pfnTxCallback != NULL)
				sUartChannel[eUART_CH1].pfnTxCallback();
		}
	}

	/* Delete all flags */
	UART_SR_WRITE(eUART_CH1, 0);
}

/*****************************************************************************************************
* \brief    USART2 Interrupt Routine Service
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
* \notes
*****************************************************************************************************/
void USART2_IRQHandler(void)
{
	/* Verify if it was a Rx interrupt */
	if(UART_SR_GET(eUART_CH2, USART_SR_RXNE))
	{
		if( sUartChannel[eUART_CH2].pu8RxBuffer != NULL &&
			sUartChannel[eUART_CH2].u16RxSize < UART_CH1_BUFFER_SIZE)
		{
			*sUartChannel[eUART_CH2].pu8RxBuffer++ = UART_DR_READ(eUART_CH2);// USART6->DR;
			sUartChannel[eUART_CH2].u16RxSize++;
			UART_CR1_SET(eUART_CH2, USART_CR1_IDLEIE);
		}
	}
	/* Verify if channel is IDLE */
	else if(UART_SR_GET(eUART_CH2, USART_SR_IDLE) )
	{
		/* Sequence to turn off IDLE flag */
		u8UartTemp = UART_DR_READ(eUART_CH2);

		/* Rx callback */
		if(sUartChannel[eUART_CH2].pfnRxCallback != NULL)
			sUartChannel[eUART_CH2].pfnRxCallback();

		UART_CR1_CLEAR(eUART_CH2, USART_CR1_IDLEIE);

	}
	/* Verify if it was a Tx interrupt. Tx Buffer empty */
	else if(UART_SR_GET(eUART_CH2, USART_SR_TXE) )
	{
		/* Review if there are more bytes to send */
		if(sUartChannel[eUART_CH2].u16TxSize-- > 0)
		{
			if(sUartChannel[eUART_CH2].pu8TxBuffer != NULL)
				UART_DR_WRITE(eUART_CH2, *sUartChannel[eUART_CH2].pu8TxBuffer++);
		}
		else
		{
			/* Turn off Tx interrupt */
			UART_CR1_CLEAR(eUART_CH2, USART_CR1_TXEIE);

			/* Change status of driver */
			sUartChannel[eUART_CH2].u8Status = UART_ON;
			sUartChannel[eUART_CH2].pu8TxBuffer = NULL;

			/* Tx callback */
			if(sUartChannel[eUART_CH2].pfnTxCallback != NULL)
				sUartChannel[eUART_CH2].pfnTxCallback();
		}
	}

	/* Delete all flags */
	UART_SR_WRITE(eUART_CH2, 0);
}

/*****************************************************************************************************
* \brief    USART3 Interrupt Routine Service
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
* \notes
*****************************************************************************************************/
void USART3_IRQHandler(void)
{
	/* Verify if it was a Rx interrupt */
	if(UART_SR_GET(eUART_CH3, USART_SR_RXNE))
	{
		if( sUartChannel[eUART_CH3].pu8RxBuffer != NULL &&
			sUartChannel[eUART_CH3].u16RxSize < UART_CH1_BUFFER_SIZE)
		{
			*sUartChannel[eUART_CH3].pu8RxBuffer++ = UART_DR_READ(eUART_CH3);// USART6->DR;
			sUartChannel[eUART_CH3].u16RxSize++;
			UART_CR1_SET(eUART_CH3, USART_CR1_IDLEIE);
		}
	}
	/* Verify if channel is IDLE */
	else if(UART_SR_GET(eUART_CH3, USART_SR_IDLE) )
	{
		/* Sequence to turn off IDLE flag */
		u8UartTemp = UART_DR_READ(eUART_CH3);

		/* Rx callback */
		if(sUartChannel[eUART_CH3].pfnRxCallback != NULL)
			sUartChannel[eUART_CH3].pfnRxCallback();

		UART_CR1_CLEAR(eUART_CH3, USART_CR1_IDLEIE);

	}
	/* Verify if it was a Tx interrupt. Tx Buffer empty */
	else if(UART_SR_GET(eUART_CH3, USART_SR_TXE) )
	{
		/* Review if there are more bytes to send */
		if(sUartChannel[eUART_CH3].u16TxSize-- > 0)
		{
			if(sUartChannel[eUART_CH3].pu8TxBuffer != NULL)
				UART_DR_WRITE(eUART_CH3, *sUartChannel[eUART_CH3].pu8TxBuffer++);
		}
		else
		{
			/* Turn off Tx interrupt */
			UART_CR1_CLEAR(eUART_CH3, USART_CR1_TXEIE);

			/* Change status of driver */
			sUartChannel[eUART_CH3].u8Status = UART_ON;
			sUartChannel[eUART_CH3].pu8TxBuffer = NULL;

			/* Tx callback */
			if(sUartChannel[eUART_CH3].pfnTxCallback != NULL)
				sUartChannel[eUART_CH3].pfnTxCallback();
		}
	}

	/* Delete all flags */
	UART_SR_WRITE(eUART_CH3, 0);
}

/***************************************End of Functions Definition**********************************/
