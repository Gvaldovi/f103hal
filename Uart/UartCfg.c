/****************************************************************************************************/
/**
    \file       UartCfg.c
  \brief
  \author       Gerardo Valdovinos
  \project      Bluepill
  \version
  \date         Mar 15, 2019

  THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                           Include files                                            *
*****************************************************************************************************/
#include "UartCfg.h"
/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #DEFINEs                                             *
*****************************************************************************************************/

/*****************************************************************************************************
*                                      Declaration of module TYPEs                                   *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        Definition of VARIABLEs                                     *
*****************************************************************************************************/
/* Rx buffers */
/* This buffer sizes defines are for not using FreeRTOS malloc in this module */
u8 au8UartCh1RxBuffer[UART_CH1_BUFFER_SIZE];
u8 au8UartCh2RxBuffer[UART_CH2_BUFFER_SIZE];
u8 au8UartCh3RxBuffer[UART_CH3_BUFFER_SIZE];

/**
 * @brief How to configure a USART channel
 *      Uart channel            Select the channel (UART_CHx , x : 1..6)
 *      Baud rate               Select baud rate depending on PCLK1 (usart 2, 3, 4, 5) or PCLK2 (usart 1, 6)
 *      Word length             Select word length. If parity is on then the length would be 9
 *      Parity                  Select parity type
 *      Size of buffers         Select the size of reception buffers
 *      IRQ						Set interrupt request routine
 *      Pin/Port				Configure Rx and Tx pins
 */

const tsUartChannelCfg sUartChannelCfg[] =
{
    {
        eUART_CH1,                       					/* Uart channel */
		eUART_LENGTH_8,       								/* Word length */
        eUART_PARITY_NO,                					/* Parity */
        (u16)UART_P2_BAUD_9600,       					/* Baud rate */

		&au8UartCh1RxBuffer[0],								/* Pointer to Rx Buffer */
        USART1_IRQn,										/* Interrupt request routine */

		{eGPIO_PORTA, 9, eGPIO_AF_PP, 0},	                /* Gpio Tx configuration */
        {eGPIO_PORTA, 10, eGPIO_IN_PU, 0}	                /* Gpio Rx configuration */
    },
//    {
//		eUART_CH2,                       					/* Uart channel */
//		eUART_LENGTH_8,       								/* Word length */
//		eUART_PARITY_NO,                					/* Parity */
//		(u16)UART_P1_BAUD_115200,       					/* Baud rate */
//
//		au8UartCh2RxBuffer,									/* Pointer to Rx Buffer */
//		USART2_IRQn,										/* Interrupt request routine */
//
//        {eGPIO_PORTA, 2, eGPIO_AF_PP_PU, GPIO_AF_USART2},	/* Gpio Tx configuration */
//        {eGPIO_PORTA, 3, eGPIO_AF_PP_PU, GPIO_AF_USART2}	/* Gpio Rx configuration */
//    },
//    {
//		eUART_CH3,                       					/* Uart channel */
//		eUART_LENGTH_9,       								/* Word length */
//		eUART_PARITY_EVEN,         							/* Parity */
//		(u16)UART_P1_BAUD_19200,        					/* Baud rate */
//
//		au8UartCh3RxBuffer,									/* Pointer to Rx Buffer */
//		USART3_IRQn,											/* Interrupt request routine */
//
//        {eGPIO_PORTD, 8, eGPIO_AF_PP_PU, GPIO_AF_USART3},	/* Gpio Tx configuration */
//        {eGPIO_PORTD, 9, eGPIO_AF_PP_PU, GPIO_AF_USART3}	/* Gpio Rx configuration */
//    }
};

const tsUartDriverCfg sUartDriverCfg =
{
    sizeof(sUartChannelCfg)/sizeof(sUartChannelCfg[0]), /* Number of channels */
    &sUartChannelCfg[0]                                 /* Channels configuration pointer */
};

/****************************************************************************************************
*                                    Declaration of module FUNCTIONs                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                         Definition of FUNCTIONS                                    *
*****************************************************************************************************/

/***************************************End of Functions Definition**********************************/
