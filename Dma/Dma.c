/****************************************************************************************************/
/**
  \file         Dma.c
  \brief        
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version      0.0.1
  \date         Aug 12, 2016

  Program compiled with "COMPILER VERSION",
  Tested on "IK MASTER" board.

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include "stm32f4xx.h"
#include "string.h"

#include "Dma.h"
/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
u8pfn u8pfnDmaCallback;
/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        Variables EXTERN                                            *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Configuration of DMA.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnDmaInit(u8 *pu8Buffer, u16 u16Size, u8pfn u8pfnCallback)
{
	DMA_InitTypeDef DMA_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	/* Copy callback */
	u8pfnDmaCallback = u8pfnCallback;

	/* Enable clock of DMA2 */
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;

    /* Configure DMA for USART RX, DMA1, Stream5, Channel4 */
    DMA_StructInit(&DMA_InitStruct);
    DMA_InitStruct.DMA_Channel = DMA_Channel_4;
    DMA_InitStruct.DMA_Memory0BaseAddr = (uint32_t)pu8Buffer;
    DMA_InitStruct.DMA_BufferSize = u16Size;
    DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&USART1->DR;
    DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStruct.DMA_Mode = DMA_Mode_Circular;
    DMA_Init(DMA2_Stream2, &DMA_InitStruct);

    /* Enable global interrupts for DMA stream */
    NVIC_InitStruct.NVIC_IRQChannel = DMA2_Stream2_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 6;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 3;
    NVIC_Init(&NVIC_InitStruct);

    /* Enable transfer complete interrupt */
    DMA_ITConfig(DMA2_Stream2, DMA_IT_TC | DMA_IT_HT, ENABLE);
    DMA_Cmd(DMA2_Stream2, ENABLE);
}

/*****************************************************************************************************
* \brief    DMA deinit.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnDmaDeInit(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;

	/* De init DMA */
    DMA_DeInit(DMA2_Stream2);

    /* Disable global interrupts for DMA stream */
	NVIC_InitStruct.NVIC_IRQChannel = DMA2_Stream2_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = DISABLE;
	NVIC_Init(&NVIC_InitStruct);

	/* Disable transfer complete interrupt */
	DMA_ITConfig(DMA2_Stream2, DMA_IT_TC | DMA_IT_HT, DISABLE);
	DMA_Cmd(DMA2_Stream2, DISABLE);
}

/*****************************************************************************************************
* \brief    DMA ISR. Callbacks with the reference of the finished block.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void DMA2_Stream2_IRQHandler(void)
{
    /* Check transfer complete flag */
    if(DMA2->LISR & DMA_FLAG_TCIF2)
    {
        DMA2->LIFCR |= DMA_FLAG_TCIF2;           /* Clear transfer complete flag */
        u8pfnDmaCallback(2);
    }
    else if(DMA2->LISR & DMA_FLAG_HTIF2)
    {
    	u8pfnDmaCallback(1);
    }
    DMA2->LIFCR = DMA_FLAG_DMEIF2 | DMA_FLAG_FEIF2 | DMA_FLAG_HTIF2 | DMA_FLAG_TCIF2 | DMA_FLAG_TEIF2;
}
/***************************************End of Functions Definition**********************************/
