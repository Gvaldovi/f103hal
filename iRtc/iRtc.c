/****************************************************************************************************/
/**
  \file         iRtc.c
  \brief		RTC internal.
  \author       Gerardo
  \project      Bluepill
  \version
  \date         10:39:49 28/03/2017 

  THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
/* Kernel includes. */
//#include "FreeRTOS.h"
//#include "task.h"
//#include "queue.h"
//#include "timers.h"
//#include "semphr.h"

#include "Utility.h"
#include "iRtc.h"
//#include <time.h>
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           Variables EXTERN                                         *
*****************************************************************************************************/
/* RTOS objects extern */
//extern QueueHandle_t xAutoQ;
//extern QueueHandle_t xMeterQ;

/*****************************************************************************************************
*                                        Definition of CONSTs                                        *
*****************************************************************************************************/

/*****************************************************************************************************
*                                      Definition of VARIABLEs                                       *
*****************************************************************************************************/
u8 u8RtcReady;

/* Messages */
//tsMsg siRtcMsg;
//tsMsg *psiRtcMsg;
//tsMsg siRtcMsg1;
//tsMsg *psiRtcMsg1;

//RTC_TimeTypeDef tRtcTime;
//RTC_DateTypeDef tRtcDate;
//time_t tRtcEpoch;
//const time_t tRtcEpoch2000 = 0x386D4380;

//struct tm *pstRtcDate;
//struct tm stRtcDate;
/*****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                      Definition of FUNCTIONS                                       *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief	Initialization of internal RTC.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfniRtcInit(void)
{
	/* Enable the peripheral clock RTC */
	/* (1) Enable PWR clock and Backup registers clock */
	/* (2) Enable write in RTC domain control register */
	/* (3) Reset RTC domain control register */
	/* (4) Enable the LSE, enable RTC and select LSE as source */
	/* (5) Wait while it is not ready */

	RCC->APB1ENR |= RCC_APB1ENR_PWREN | RCC_APB1ENR_BKPEN; /* (1) */
	PWR->CR |= PWR_CR_DBP; /* (2) */
	RCC->BDCR |= RCC_BDCR_BDRST; /* (3) */
	RCC->BDCR = RCC_BDCR_RTCEN | RCC_BDCR_RTCSEL_0 | RCC_BDCR_LSEON; /* (4) */
	while((RCC->BDCR & RCC_BDCR_LSERDY) != RCC_BDCR_LSERDY) /* (5) */
	{
		/* add time out here for a robust application */
	}

	/* Enable RTC */
	//	1. Poll RTOFF, wait until its value goes to �1�
	//	2. Set the CNF bit to enter configuration mode
	//	3. Write to one or more RTC registers
	//	4. Clear the CNF bit to exit configuration mode
	//	5. Poll RTOFF, wait until its value goes to �1� to check the end of the write operation.
	while((RTC->CRL & RTC_CRL_RTOFF) != RTC_CRL_RTOFF)
	{
		/* add time out here for a robust application */
	}
	RTC->CRL |= RTC_CRL_CNF;
	RTC->PRLL = 0x7FFF;
	/* 17/03/19 1:55pm Mexico without DTS */
	RTC->CNTH = 0x5C8E;
	RTC->CNTL = 0xA60A;
	RTC->CRL &= ~RTC_CRL_CNF;
	while((RTC->CRL & RTC_CRL_RTOFF) != RTC_CRL_RTOFF)
	{
		/* add time out here for a robust application */
	}

	/* Configure EXTI line 17. This line is for RTC alarms */
	EXTI->IMR |= EXTI_IMR_MR17;
	EXTI->RTSR |= EXTI_RTSR_TR17;
}

/*****************************************************************************************************
* \brief    Set epoch in RTC.
* \author   Gerardo Valdovinos Villalobos
* \param    Epoch time pointer
* \return   void
*****************************************************************************************************/
void vfniRtcAlarmSet(u16 u16AlarmTime)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	u32 u32Epoch;

	/* Read current epoch and add the alarm time */
	u32Epoch = (u32)((RTC->CNTH << 16) + (RTC->CNTL));
	if((u32Epoch + u16AlarmTime) <= 0xFFFFFFFF)
		u32Epoch += u16AlarmTime;

	/* Clear EXTI and Alarm A flag */
	EXTI->PR |= EXTI_PR_PR17;

	/* Enable RTC alarm */
	//	1. Poll RTOFF, wait until its value goes to �1�
	//	2. Set the CNF bit to enter configuration mode
	//	3. Write to one or more RTC registers
	//	4. Clear the CNF bit to exit configuration mode
	//	5. Poll RTOFF, wait until its value goes to �1� to check the end of the write operation.
	while((RTC->CRL & RTC_CRL_RTOFF) != RTC_CRL_RTOFF)
	{
		/* add time out here for a robust application */
	}
	RTC->CRL |= RTC_CRL_CNF;
	/* Set alarm */
	RTC->ALRH = (u16)((u32Epoch & 0xFFFF0000) >> 16);
	RTC->ALRL = (u16)((u32Epoch & 0x0000FFFF) >> 0);
	/* Activate alarm */
	RTC->CRH |= RTC_CRH_ALRIE;
	RTC->CRL &= ~RTC_CRL_CNF;
	while((RTC->CRL & RTC_CRL_RTOFF) != RTC_CRL_RTOFF)
	{
		/* add time out here for a robust application */
	}

	/* Activate interrupts */
	NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 9;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/*****************************************************************************************************
* \brief    Set epoch in RTC.
* \author   Gerardo Valdovinos Villalobos
* \param    Epoch time pointer
* \return   void
*****************************************************************************************************/
void vfniRtcSetEpoch(u8 *pu8Epoch)
{
	u32 u32Time;

    if(pu8Epoch != NULL)
    {
//		/* Copy Epoch 2000 */
//		tRtcEpoch  = (time_t)(pu8Epoch[0] << 24);
//		tRtcEpoch |= (time_t)(pu8Epoch[1] << 16);
//		tRtcEpoch |= (time_t)(pu8Epoch[2] << 8);
//		tRtcEpoch |= (time_t)(pu8Epoch[3] << 0);
//
//		/* Convert to normal epoch. Add seconds from 1970 to 2000 year */
//		tRtcEpoch += tRtcEpoch2000;
//
//		/* Convert epoch to Date and time */
//		pstRtcDate = localtime(&tRtcEpoch);
//
//		u32Time  = RTC_HEX2BCD_SEC(pstRtcDate->tm_sec);
//		u32Time |= RTC_HEX2BCD_MIN(pstRtcDate->tm_min);
//		u32Time |= RTC_HEX2BCD_HOUR(pstRtcDate->tm_hour);
//
//		u32Date  = RTC_HEX2BCD_DAY(pstRtcDate->tm_mday);
//		u32Date |= RTC_HEX2BCD_MON(pstRtcDate->tm_mon);
//		u32Date |= RTC_HEX2BCD_WDAY(pstRtcDate->tm_wday);
//		u32Date |= RTC_HEX2BCD_YEAR(pstRtcDate->tm_year);


		/* Copy Epoch */
    	u32Time  = (u32)(pu8Epoch[0] << 24);
    	u32Time |= (u32)(pu8Epoch[1] << 16);
    	u32Time |= (u32)(pu8Epoch[2] << 8);
    	u32Time |= (u32)(pu8Epoch[3] << 0);

    	/* Enable RTC */
    	//	1. Poll RTOFF, wait until its value goes to �1�
    	//	2. Set the CNF bit to enter configuration mode
    	//	3. Write to one or more RTC registers
    	//	4. Clear the CNF bit to exit configuration mode
    	//	5. Poll RTOFF, wait until its value goes to �1� to check the end of the write operation.
    	while((RTC->CRL & RTC_CRL_RTOFF) != RTC_CRL_RTOFF)
    	{
    		/* add time out here for a robust application */
    	}
    	RTC->CRL |= RTC_CRL_CNF;
    	/* Change epoch */
    	RTC->CNTH = (u16)((u32Time & 0xFFFF0000) >> 16);
    	RTC->CNTL = (u16)((u32Time & 0x0000FFFF) >> 0);
    	RTC->CRL &= ~RTC_CRL_CNF;
    	while((RTC->CRL & RTC_CRL_RTOFF) != RTC_CRL_RTOFF)
    	{
    		/* add time out here for a robust application */
    	}

		/* RTC set succesfull */
		u8RtcReady = 1;
    }
}

/*****************************************************************************************************
* \brief    Get time in epoch from RTC.
* \author   Gerardo Valdovinos Villalobos
* \param    Epoch time pointer
* \return   void
*****************************************************************************************************/
u8 u8fniRtcGetEpoch(u8 *pu8Epoch)
{
	u32 u32Time;

	if(u8RtcReady == 0)
		return FALSE;

    if(pu8Epoch != NULL)
    {
//		/* Get RTC time */
//		u32Time = RTC->TR;
//		u32Date = RTC->DR;
//
//		/* Fill tm struct */
//		stRtcDate.tm_sec = RTC_BCD2HEX_SEC(u32Time);
//		stRtcDate.tm_min = RTC_BCD2HEX_MIN(u32Time);
//		stRtcDate.tm_hour = RTC_BCD2HEX_HOUR(u32Time);
//
//		stRtcDate.tm_mday = RTC_BCD2HEX_DAY(u32Date);
//		stRtcDate.tm_mon = RTC_BCD2HEX_MON(u32Date);
//		stRtcDate.tm_wday = RTC_BCD2HEX_WDAY(u32Date);
//		stRtcDate.tm_year = RTC_BCD2HEX_YEAR(u32Date);
//
//		tRtcEpoch = mktime(&stRtcDate);
//
//		/* Get epoch 2000 */
//		tRtcEpoch -= tRtcEpoch2000;
//
    	u32Time = (u32)((RTC->CNTH << 16) + (RTC->CNTL));
		pu8Epoch[0] = (u32Time & 0xFF000000) >> 24;
		pu8Epoch[1] = (u32Time & 0x00FF0000) >> 16;
		pu8Epoch[2] = (u32Time & 0x0000FF00) >> 8;
		pu8Epoch[3] = (u32Time & 0x000000FF) >> 0;

		return TRUE;
    }
    else
    	return FALSE;
}

/*****************************************************************************************************
* \brief	Alarm from RTC.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void RTC_IRQHandler(void)
{
	/* Clear EXTI and Alarm A flag */
	EXTI->PR |= (1 << 17);

//	/* Read the minute */
//	u8Minute = (((RTC->TR) & 0x0000FF00) >> 8);
//	u8Minute = RTC_BCD_TO_DEC(u8Minute);
//
//	/* Activate reading for meters every 15 min */
//	if( (u8Minute % u8ReadingsTime) == 0)
//	{
//		/* Readings */
//		psiRtcMsg->sInfo.eLocalCmd = eCMD_AUTO_READINGS;
//		xQueueSendFromISR( xAutoQ, (void *)&psiRtcMsg, 0 );
//	}
//	else if((u8Minute % u8ReadingsTime) == 2)
//	{
//		/* Readings only from meters that failed before */
//		psiRtcMsg->sInfo.eLocalCmd = eCMD_AUTO_READINGS_ERROR;
//		xQueueSendFromISR( xAutoQ, (void *)&psiRtcMsg, 0 );
//	}
//	else
//	{
//		/* Alarms */
//		psiRtcMsg->sInfo.eLocalCmd = eCMD_AUTO_ALARMS;
//		xQueueSendFromISR( xAutoQ, (void *)&psiRtcMsg, 0 );
//	}
//
//	/* Automatic Discovery/Recovery */
//	if((u8Minute % 10) == 0)
//	{
//		if(u8RtcDiscovery)
//		{
//			/* Readings only from meters that failed before */
//			psiRtcMsg1->sInfo.eLocalCmd = eCMD_METER_DISCOVERY;
//			xQueueSendFromISR( xMeterQ, (void *)&psiRtcMsg1, 0 );
//		}
//		else
//		{
//			/* Readings only from meters that failed before */
//			psiRtcMsg1->sInfo.eLocalCmd = eCMD_METER_RECOVERY;
//			xQueueSendFromISR( xMeterQ, (void *)&psiRtcMsg1, 0 );
//			u8RtcDiscovery = TRUE;
//		}
//	}
}
/***************************************End of File**************************************************/
