/****************************************************************************************************/
/**
  \file         iRtc.h
  \brief		RTC internal.
  \author       Gerardo
  \project      Bluepill
  \version
  \date         10:40:00 28/03/2017 

  THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/
#ifndef __IRTC_H
#define	__IRTC_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/
#define RTC_BCD_TO_DEC(x)			(((x & 0x70) >> 4) * 10) + (x & 0x0F)

#define RTC_HEX2BCD_SEC(x)			((u32)(_vfniRtcHex2Bcd(x) << 0) & 0x0000007F)
#define RTC_HEX2BCD_MIN(x)			((u32)(_vfniRtcHex2Bcd(x) << 8) & 0x00007F00)
#define RTC_HEX2BCD_HOUR(x)			((u32)(_vfniRtcHex2Bcd(x) << 16) & 0x003F0000)

#define RTC_HEX2BCD_DAY(x)			((u32)(_vfniRtcHex2Bcd(x) << 0) & 0x0000003F)
#define RTC_HEX2BCD_MON(x)			((u32)(_vfniRtcHex2Bcd(x + 1) << 8) & 0x00001F00)
#define RTC_HEX2BCD_WDAY(x)			((u32)(_vfniRtcHex2Bcd(x) << 13) & 0x0000E000)
#define RTC_HEX2BCD_YEAR(x)			((u32)(_vfniRtcHex2Bcd(x - 100) << 16) & 0x00FF0000)

#define RTC_BCD2HEX_SEC(x)			_vfniRtcBcd2Hex((u8)((x & 0x0000007F) >> 0));
#define RTC_BCD2HEX_MIN(x)			_vfniRtcBcd2Hex((u8)((x & 0x00007F00) >> 8));
#define RTC_BCD2HEX_HOUR(x)			_vfniRtcBcd2Hex((u8)((x & 0x003F0000) >> 16));

#define RTC_BCD2HEX_DAY(x)			_vfniRtcBcd2Hex((u8)((x & 0x0000003F) >> 0));
#define RTC_BCD2HEX_MON(x)			_vfniRtcBcd2Hex((u8)((x & 0x00001F00) >> 8)) - 1;
#define RTC_BCD2HEX_WDAY(x)			_vfniRtcBcd2Hex((u8)((x & 0x0000E000) >> 13));
#define RTC_BCD2HEX_YEAR(x)			_vfniRtcBcd2Hex((u8)((x & 0x00FF0000) >> 16)) + 100;
/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/

/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfniRtcInit(void);
void vfniRtcAlarmSet(u16 u16AlarmTime);
void vfniRtcSetEpoch(u8 *pu8Epoch);
u8 u8fniRtcGetEpoch(u8 *pu8Epoch);

#endif	/* __IRTC_H */
/***************************************End of File**************************************************/
